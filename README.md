D3M Interface Library
=====================
Library to use D3M AutoML systems. This repository contains an implementation to integrate 
 D3M AutoML systems with Jupyter Notebooks. 

[Documentation is available here](https://d3m-interface.readthedocs.io/en/latest/)

## Installation
This package works with Python 3.6. You need to have [Docker](https://docs.docker.com/get-docker/) installed on your operating system.

You can install the latest stable version of this library from [PyPI](https://pypi.org/project/d3m-interface/):

```
$ pip3 install d3m-interface
```

To install the latest development version:

```
$ pip3 install git+https://gitlab.com/ViDA-NYU/d3m/d3m_interface.git
```
