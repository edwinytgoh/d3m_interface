Installation
============

Linux/Mac
---------

This package works with Python 3.6. You need to have `Docker <https://docs.docker.com/get-docker/>`__ installed on your operating system.

You can install the latest stable version of this library directly from `PyPI <https://pypi.org/project/d3m-interface/>`__ using PIP::

    $ pip3 install d3m-interface

To install the latest development version::

    $ pip3 install git+https://gitlab.com/ViDA-NYU/d3m/d3m_interface.git

Windows
---------

T.B.D